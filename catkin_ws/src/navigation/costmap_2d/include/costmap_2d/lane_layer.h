#ifndef COSTMAP_2D_STATIC_LAYER_H_
#define COSTMAP_2D_STATIC_LAYER_H_

#include <ros/ros.h>
#include <costmap_2d/costmap_layer.h>
#include <costmap_2d/layered_costmap.h>
#include <costmap_2d/GenericPluginConfig.h>
#include <dynamic_reconfigure/server.h>
#include <nav_msgs/OccupancyGrid.h>
#include <message_filters/subscriber.h>

namespace costmap_2d
{

class LaneLayer : public CostmapLayer
{
public:
	LaneLayer();
	virtual ~LaneLayer();
	virtual void onInitialize();
	virtual void activate();
	virtual void deactivate();
	virtual void reset();

	virtual void updateBounds(double, double, double, double*, double*, double*, double*);
	virtual void updateCosts(costmap_2d::Costmap2D&, int, int, int, int);

	virtual void matchSize();

private:
	void incomingMap(const nav_msgs::OccupancyGridConstPtr& new_map);
	void reconfigureCB(costmap_2d::GenericPluginConfig &, uint32_t);

	std::string global_frame_;
	std::string map_frame_;
	bool map_received_;
	bool has_updated_data_;
	bool use_maximum_;
	bool first_map_only_;
	bool subscribe_to_updates_;

	ros::Subscriber map_sub_;
	unsigned int x_, y_, width_, height_; 
	int lane_cost_, default_lane_cost_;

	dynamic_reconfigure::Server<costmap_2d::GenericPluginConfig> *dsrv_;
};
}
#endif
