/*
 * Written by Dheeraj R. Reddy
 * 19th May 2017
 * 
 * Makes the road ahead fully empty, ie, zero cost
 * Road grid is published on topic -- "road_grid"
 *
 * Params:
 * 	road_cost -- sets the cost of the road, deafault: 0
 */
#include <costmap_2d/road_layer.h>
#include <costmap_2d/costmap_math.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(costmap_2d::RoadLayer, costmap_2d::Layer)

using costmap_2d::FREE_SPACE;
using costmap_2d::LETHAL_OBSTACLE;

namespace costmap_2d
{

RoadLayer::RoadLayer() : dsrv_(NULL) {}

RoadLayer::~RoadLayer()
{
	if(dsrv_)
		delete dsrv_;
}

void RoadLayer::onInitialize()
{
	ros::NodeHandle nh("~/" + name_), g_nh;
	current_ = true;

	global_frame_ = layered_costmap_->getGlobalFrameID();
	
	std::string map_topic;
	nh.param("map_topic", map_topic, std::string("road_grid"));
	nh.param("first_map_only", first_map_only_, false);
	nh.param("subscribe_to_updates", subscribe_to_updates_, false);
	nh.param("road_cost", road_cost_, 100);

	if(map_sub_.getTopic() != ros::names::resolve(map_topic))
	{
		map_sub_ = g_nh.subscribe(map_topic, 1, &RoadLayer::incomingMap, this);
		map_received_ = false;
		has_updated_data_ = false;

		ros::Rate r(10);
		while(!map_received_ && g_nh.ok())
		{
			ros::spinOnce();
			r.sleep();
		}
	}
	else has_updated_data_ = true;
	if(dsrv_) delete dsrv_;

	dsrv_ = new dynamic_reconfigure::Server<costmap_2d::GenericPluginConfig>(nh);
	dynamic_reconfigure::Server<costmap_2d::GenericPluginConfig>::CallbackType cb = boost::bind(&RoadLayer::reconfigureCB, this, _1, _2);
	dsrv_->setCallback(cb);
}

void RoadLayer::reconfigureCB(costmap_2d::GenericPluginConfig &config, uint32_t level)
{
	if(config.enabled != enabled_) 
	{
		enabled_ = config.enabled;
		has_updated_data_ = true;
		x_ = y_ = 0;
		width_ = size_x_;
		height_ = size_y_;
	}
}

void RoadLayer::matchSize()
{
	if(!layered_costmap_->isRolling())
	{
		Costmap2D* master = layered_costmap_->getCostmap();
		resizeMap(master->getSizeInCellsX(), master->getSizeInCellsY(), master->getResolution(), master->getOriginX(), master->getOriginY());
	}
}

void RoadLayer::incomingMap(const nav_msgs::OccupancyGridConstPtr& new_map)
{
	unsigned int size_x = new_map -> info.width;
	unsigned int size_y = new_map -> info.height;

	Costmap2D* master = layered_costmap_ -> getCostmap();
	if(!layered_costmap_->isRolling() && (master->getSizeInCellsX() != size_x ||
			master->getSizeInCellsY() != size_y ||
			master->getResolution() != new_map->info.resolution ||
			master->getOriginX() != new_map->info.origin.position.x ||
			master->getOriginY() != new_map->info.origin.position.y ||
			!layered_costmap_->isSizeLocked()))
	{
		layered_costmap_->resizeMap(size_x, size_y, new_map->info.resolution, new_map->info.origin.position.x, new_map->info.origin.position.y, true);
	}
	else if(size_x_ != size_x || size_y_ != size_y ||
			resolution_ != new_map->info.resolution ||
			origin_x_ != new_map->info.origin.position.x ||
			origin_y_ != new_map->info.origin.position.y)
	{
		resizeMap(size_x, size_y, new_map->info.resolution, new_map->info.origin.position.x, new_map->info.origin.position.y);
	}

	unsigned int index = 0;

	for(int i = 0; i < size_y; i++)
	{
		for(int j = 0; j < size_x; j++)
		{
			unsigned char value = new_map->data[index];
			costmap_[index] = road_cost_;
			index++;
		}
	}
	map_frame_ = new_map->header.frame_id;

	x_ = y_ = 0;
	width_ = size_x_;
	height_ = size_y_;
	map_received_ = true;
	has_updated_data_ = true;
}

void RoadLayer::activate()
{
	onInitialize();
}

void RoadLayer::deactivate()
{
	map_sub_.shutdown();
}

void RoadLayer::reset()
{
	onInitialize();
}

void RoadLayer::updateBounds(double robot_x, double robot_y, double robot_yaw, double* min_x, double* min_y, double* max_x, double* max_y)
{
	if(!layered_costmap_->isRolling())
		if(!map_received_ || !(has_updated_data_ || has_extra_bounds_))
			return;

	useExtraBounds(min_x, min_y, max_x, max_y);
	
	double wx, wy;

	mapToWorld(x_, y_, wx, wy);
	*min_x = std::min(*min_x, wx);
	*min_y = std::min(*min_y, wy);

	mapToWorld(x_ + width_, y_ + height_, wx, wy);
	*max_x = std::max(*max_x, wx);
	*max_y = std::max(*max_y, wy);

	has_updated_data_ = false;
}

void RoadLayer::updateCosts(costmap_2d::Costmap2D& master_grid, int min_i, int min_j, int max_i, int max_j)
{
	if(!map_received_) return;

	if(!layered_costmap_->isRolling())
	{
		updateWithTrueOverwrite(master_grid, min_i, min_j, max_i, max_j);
	}
	else
	{
		unsigned int mx, my;
		double wx, wy;

		tf::StampedTransform transform;
		try
		{
			tf_->lookupTransform(map_frame_, global_frame_, ros::Time(0), transform);
		}
		catch(tf::TransformException ex)
		{
			ROS_ERROR("%s", ex.what());
			return;
		}

		for(int i = min_i; i < max_i; i++)
		{
			for(int j = min_j; j < max_j; j++)
			{
				layered_costmap_->getCostmap()->mapToWorld(i, j, wx, wy);
				tf::Point p(wx, wy, 0);
				if(worldToMap(p.x(), p.y(), mx, my))
				{
					master_grid.setCost(i, j, road_cost_);
				}
			}
		}
	}
}

}
